/*HW10 by Alan Zarza
 *play one game of tic tac toe 
 *with a friend
 */
import java.util.Scanner;

public class HW10{
  public static int Xinput(Scanner scan){
    int number, count = 0;//declaring number and counter/ initializing count
    do{//do while so we always do do the loop at least once
      if(count == 0){//if this is the first iteration
        System.out.println("Player 1: Please enter a number between 1-9");//print this message
      }
      else{//all other times are an error
        System.out.println("Error: Please enter a number between 1-9");//so print this message
      }
      while(!scan.hasNextInt()){//as long as the input isn't an integer
        System.out.println("Error: Please enter an integer");//print error message
        scan.next();//move on to next scan
      }
      number = scan.nextInt();//set number as the input
      count += 1;//increment count
    }while(number<1||number>9);//keep doing that loop as long as we don't have a number in correct range
    return number;//when we get acceptable input, return the number
  }//asks and check for integer input between 1-9 for player 1
  public static int Oinput(Scanner scan){
    int number, count = 0;//declaring number and counter/ initializing count
    do{//do while so we always do do the loop at least once
      if(count == 0){//if this is the first iteration
        System.out.println("Player 2: Please enter a number between 1-9");//print this message
      }
      else{//all other times are an error
        System.out.println("Error: Please enter a number between 1-9");//so print this message
      }
      while(!scan.hasNextInt()){//as long as the input isn't an integer
        System.out.println("Error: Please enter an integer");//print error message
        scan.next();//move on to next scan
      }
      number = scan.nextInt();//set number as the input
      count += 1;//increment count
    }while(number<1||number>9);//keep doing that loop as long as we don't have a number in correct range
    return number;//when we get acceptable input, return the number
  }//asks and check for integer input between 1-9 for player 2
  public static void printBoard(int[][] board){
    for(int rows = 0; rows < board.length; rows ++){//for each element in the rows
      for(int cols = 0; cols < board[rows].length; cols++){//for each elemtn in the columns
        if(board[rows][cols] == -1){System.out.print("X ");}//if element is -1, print an X
        else if(board[rows][cols] == -2){System.out.print("O ");}//if elemnt is -2, print an O
        else{System.out.print(board[rows][cols] + " ");}//otherwise, print the element value
      }
      System.out.println();//go on to new row
    }
  }//prints the game board with X's and O's in the right places
  public static void XplacePiece(int pos, int[][] board, Scanner scan){
    boolean check = false;//start of believeing there is no position corrisponding to pos
    for(int row = 0; row < board.length; row ++){//for each row
      for(int col = 0; col < board[row].length; col ++){//and each column
        if(pos == board[row][col]){//check if the element is the same as position
          board[row][col] = -1;//if it is, then change that number to -1
          check = true;//and set the check as true since we did find a position
        }
      }
    }
    if(check == false){//if we didn't find a position
      System.out.println("That spot is taken, please choose another position");//print this message
      XplacePiece(Xinput(scan), board, scan);//call on itself until it all works out
    }
  }//places piece on board for player 1, if position is not availale, ask for new input
  public static void OplacePiece(int pos, int[][] board, Scanner scan){
    boolean check = false;//start of believeing there is no position corrisponding to pos
    for(int row = 0; row < board.length; row ++){//for each row
      for(int col = 0; col < board[row].length; col ++){//and each column
        if(pos == board[row][col]){//check if the element is the same as position
          board[row][col] = -2;//if it is, then change that number to -2
          check = true;//and set the check as true since we did find a position
        }
      }
    }
    if(check == false){//if we didn't find a position
      System.out.println("That spot is taken, please choose another position");//print this message
      OplacePiece(Oinput(scan), board, scan);//call on itself until it all works out
    }
  }//places piece on board for player 2, if position is not availale, ask for new input
  public static boolean checkWin(int[][] board){
    for(int row = 0; row < board.length; row ++){//check each row
      if(board[row][0]==-1&&board[row][1]==-1&&board[row][2]==-1){//if the whole row is equal
        System.out.println("Player 1 wins!");
        return true;//x wins
      }
    }
    for(int col = 0; col < 3; col ++){//check each column
      if(board[0][col]==-1&&board[1][col]==-1&&board[2][col]==-1){//if the whole column is equal
        System.out.println("Player 1 wins!");
        return true;//x wins
      }
    }
    if(board[0][0]==-1&&board[1][1]==-1&&board[2][2]==-1){//diagonals are equal
      System.out.println("Player 1 wins!");
      return true;//x wins
    }
    if(board[0][2]==-1&&board[1][1]==-1&&board[2][0]==-1){//if diagonals are equal
      System.out.println("Player 1 wins!");
      return true;//x wins
    }
    for(int row = 0; row < board.length; row ++){//check each row
      if(board[row][0]==-2&&board[row][1]==-2&&board[row][2]==-2){//if the whole row is equal
        System.out.println("Player 2 wins!");
        return true;//o wins
      }
    }
    for(int col = 0; col < 3; col ++){//check each column
      if(board[0][col]==-2&&board[1][col]==-2&&board[2][col]==-2){//if the whole column is equal
        System.out.println("Player 2 wins!");
        return true;//x wins
      }
    }
    if(board[0][0]==-2&&board[1][1]==-2&&board[2][2]==-2){//diagonals are equal
      System.out.println("Player 2 wins!");
      return true;//x wins
    }
    if(board[0][2]==-2&&board[1][1]==-2&&board[2][0]==-2){//if diagonals are equal
      System.out.println("Player 2 wins!");
      return true;//x wins
    }
    return false;//no win yet
  }//check all possible win scennarios
  public static void main(String []args){
    Scanner scan = new Scanner(System.in);
    
    int[][] board = {{1,2,3},
                     {4,5,6},
                     {7,8,9}};//the game board at the start of game
   
    int counter = 0;//for keeping track of how many moves have happened
    while(true){//does stuff until something breaks from the loop
      printBoard(board);//updates board
      XplacePiece(Xinput(scan), board, scan);//places piece
      if(checkWin(board)){//checks for a win
        break;//if theres a win, leave the loop
      }
      counter +=1;//increment counter since player one turn is over
      if(counter == 9){//if we reach 9 turns
        System.out.println("Draw!!!");//then the game is a draw
        break;//and leave the loop
      }
      printBoard(board);//update board
      OplacePiece(Oinput(scan), board, scan);//places player 2 piece
      if(checkWin(board)){//check for a win
        break;//if theres a win, leave the loop
      }
      counter += 1;//increment at end of player 2 turn
    }
    printBoard(board);//at end of game, print the final board
    
    
  }//end main method
}//end class