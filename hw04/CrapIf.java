//Crap If by Alan Zara
//rolls two die and determines
//slang terminology for the roll
//using only if and if-else statements
import java.util.Scanner;//importing the scanner utility

public class CrapIf{//CrapIf class
  
  public static void main(String[]args){//main method
    
    Scanner myScanner = new Scanner(System.in);//declaring instance of scanner object
    
    System.out.print("Would you like randomly cast dice, or do you want to state two dice?\n(Type 1 for random dice, 0 for your own dice)");
    int ans = myScanner.nextInt();
    
    if(ans == 1){//They want random numbers
      
      int dice1 = (int)(Math.random()*6)+1;//generates number betwween 1 and 6
      int dice2 = (int)(Math.random()*6)+1;//generates number between 1 and 6
      
      if(dice1 == 1){//When dice1 equals 1
        if(dice2 == 1){
          System.out.print("Snake Eyes");
        }
        else if(dice2 == 2){
          System.out.print("Ace Deuce");
        }
        else if(dice2 == 3){
          System.out.print("Easy Four");
        }
        else if(dice2 == 4){
          System.out.print("Fever Five");
        }
        else if(dice2 == 5){
          System.out.print("Easy Six");
        }
        else if(dice2 == 6){
          System.out.print("Seven Out");
        }
      }//end of first if statement
      
      else if(dice1 == 2){//when dice1 is 2
        if(dice2 == 1){
          System.out.print("Ace Deuce");
        }
        else if(dice2 == 2){
          System.out.print("Hard Four");
        }
        else if(dice2 == 3){
          System.out.print("Fever Five");
        }
        else if(dice2 == 4){
          System.out.print("Easy Six");
        }
        else if(dice2 == 5){
          System.out.print("Seven Out");
        }
        else if(dice2 == 6){
          System.out.print("Easy Eight");
        }
      }//end second if statement
      
      else if(dice1 == 3){//when dice1 is 3
        if(dice2 == 1){
          System.out.print("Easy Four");
        }
        else if(dice2 == 2){
          System.out.print("Fever Five");
        }
        else if(dice2 == 3){
          System.out.print("Hard Six");
        }
        else if(dice2 == 4){
          System.out.print("Seven Out");
        }
        else if(dice2 == 5){
          System.out.print("Easy Eight");
        }
        else if(dice2 == 6){
          System.out.print("Nine");
        }
      }//end third if statement
      
      else if(dice1 == 4){//when dice1 is 4
        if(dice2 == 1){
          System.out.print("Fever Five");
        }
        else if(dice2 == 2){
          System.out.print("Easy Six");
        }
        else if(dice2 == 3){
          System.out.print("Seven Out");
        }
        else if(dice2 == 4){
          System.out.print("Hard Eight");
        }
        else if(dice2 == 5){
          System.out.print("Nine");
        }
        else if(dice2 == 6){
          System.out.print("Easy Ten");
        }
      }//end fourth if statement
      
      else if(dice1 == 5){//when dice1 is 5
        if(dice2 == 1){
          System.out.print("Easy Six");
        }
        else if(dice2 == 2){
          System.out.print("Seven Out");
        }
        else if(dice2 == 3){
          System.out.print("Easy Eight");
        }
        else if(dice2 == 4){
          System.out.print("Nine");
        }
        else if(dice2 == 5){
          System.out.print("Hard Ten");
        }
        else if(dice2 == 6){
          System.out.print("Yo-leven");
        }
      }//end fifth if statement
      
       else if(dice1 == 6){//when dice1 is 6
        if(dice2 == 1){
          System.out.print("Seven Out");
        }
        else if(dice2 == 2){
          System.out.print("Easy Eight");
        }
        else if(dice2 == 3){
          System.out.print("Nine");
        }
        else if(dice2 == 4){
          System.out.print("Easy Ten");
        }
        else if(dice2 == 5){
          System.out.print("Yo-leven");
        }
        else if(dice2 == 6){
          System.out.print("Boxcars");
        }
      }//end sixth if statement
      
    }//end random numbers
    
    else if(ans == 0){//They have two numbers
      
      System.out.print("Number 1:");//asking for first number
      int dice1 = myScanner.nextInt();
      System.out.print("Number 2:");//asking for second number
      int dice2 = myScanner.nextInt();
      
      if(!((dice1 >= 1 && dice1 <=6)) || !((dice2 >= 1 && dice2 <=6))){//if any of the numbers is not between 1-6, then do this
        System.out.print("One or more of your numbers is not within the range 1-6\n" +
                        "Restart program to try again");
      }
      else{//if numbers are in the correct range
        if(dice1 == 1){//When dice1 equals 1
        if(dice2 == 1){
          System.out.print("Snake Eyes");
        }
        else if(dice2 == 2){
          System.out.print("Ace Deuce");
        }
        else if(dice2 == 3){
          System.out.print("Easy Four");
        }
        else if(dice2 == 4){
          System.out.print("Fever Five");
        }
        else if(dice2 == 5){
          System.out.print("Easy Six");
        }
        else if(dice2 == 6){
          System.out.print("Seven Out");
        }
      }//end of first if statement
      
      else if(dice1 == 2){//when dice1 is 2
        if(dice2 == 1){
          System.out.print("Ace Deuce");
        }
        else if(dice2 == 2){
          System.out.print("Hard Four");
        }
        else if(dice2 == 3){
          System.out.print("Fever Five");
        }
        else if(dice2 == 4){
          System.out.print("Easy Six");
        }
        else if(dice2 == 5){
          System.out.print("Seven Out");
        }
        else if(dice2 == 6){
          System.out.print("Easy Eight");
        }
      }//end second if statement
      
      else if(dice1 == 3){//when dice1 is 3
        if(dice2 == 1){
          System.out.print("Easy Four");
        }
        else if(dice2 == 2){
          System.out.print("Fever Five");
        }
        else if(dice2 == 3){
          System.out.print("Hard Six");
        }
        else if(dice2 == 4){
          System.out.print("Seven Out");
        }
        else if(dice2 == 5){
          System.out.print("Easy Eight");
        }
        else if(dice2 == 6){
          System.out.print("Nine");
        }
      }//end third if statement
      
      else if(dice1 == 4){//when dice1 is 4
        if(dice2 == 1){
          System.out.print("Fever Five");
        }
        else if(dice2 == 2){
          System.out.print("Easy Six");
        }
        else if(dice2 == 3){
          System.out.print("Seven Out");
        }
        else if(dice2 == 4){
          System.out.print("Hard Eight");
        }
        else if(dice2 == 5){
          System.out.print("Nine");
        }
        else if(dice2 == 6){
          System.out.print("Easy Ten");
        }
      }//end fourth if statement
      
      else if(dice1 == 5){//when dice1 is 5
        if(dice2 == 1){
          System.out.print("Easy Six");
        }
        else if(dice2 == 2){
          System.out.print("Seven Out");
        }
        else if(dice2 == 3){
          System.out.print("Easy Eight");
        }
        else if(dice2 == 4){
          System.out.print("Nine");
        }
        else if(dice2 == 5){
          System.out.print("Hard Ten");
        }
        else if(dice2 == 6){
          System.out.print("Yo-leven");
        }
      }//end fifth if statement
      
       else if(dice1 == 6){//when dice1 is 6
        if(dice2 == 1){
          System.out.print("Seven Out");
        }
        else if(dice2 == 2){
          System.out.print("Easy Eight");
        }
        else if(dice2 == 3){
          System.out.print("Nine");
        }
        else if(dice2 == 4){
          System.out.print("Easy Ten");
        }
        else if(dice2 == 5){
          System.out.print("Yo-leven");
        }
        else if(dice2 == 6){
          System.out.print("Boxcars");
        }
      }//end sixth if statement
      }
    }//end of chosen numbers
    else{
      System.out.print("Error");
    }
  }//end main method
}//end class