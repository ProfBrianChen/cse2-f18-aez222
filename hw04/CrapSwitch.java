//Crap Switch by Alan Zara
//rolls two die and determines
//slang terminology for the roll
//using only switches
import java.util.Scanner;//importing the scanner utility

public class CrapSwitch{//CrapSwitch class
  
  public static void main(String[]args){//main method
    
    Scanner myScanner = new Scanner(System.in);//declaring instance of scanner object
    
    System.out.print("Would you like randomly cast dice, or do you want to state two dice?\n(Type 1 for random dice, 0 for your own dice)");
    int ans = myScanner.nextInt();
    
    switch(ans){
      case 1://generate two random numbers
        
        int dice1 = (int)(Math.random()*6)+1;//generates number between 1 and 6
        int dice2 = (int)(Math.random()*6)+1;//generates number between 1 and 6
        int sum = dice1 * 10 + dice2;//sums the numbers to easier identify them(16 = 1 and 6)
        String name = "";
        
        switch(sum){
          case 11:name = "Snake Eyes";
            break;
          case 12:name = "Ace Deuce";
            break;
          case 13:name = "Easy Four";
            break;
          case 14:name = "Fever Five";
            break;
          case 15:name = "Easy Six";
            break;
          case 16:name = "Seven Out";
            break;
            
          case 21:name = "Ace Deuce";
            break;
          case 22:name = "Hard Four";
            break;
          case 23:name = "Fever Five";
            break;
          case 24:name = "Easy Six";
            break;
          case 25:name = "Seven Out";
            break;
          case 26:name = "Easy Eight";
            break;
            
          case 31:name = "Easy Four";
            break;
          case 32:name = "Fever Five";
            break;
          case 33:name = "Hard Six";
            break;
          case 34:name = "Seven Out";
            break;
          case 35:name = "Easy Eight";
            break;
          case 36:name = "Nine";
            break;
            
          case 41:name = "Fever Five";
            break;
          case 42:name = "Easy Six";
            break;
          case 43:name = "Seven Out";
            break;
          case 44:name = "Hard Eight";
            break;
          case 45:name = "Nine";
            break;
          case 46:name = "Easy Ten";
            break;
            
          case 51:name = "Easy Six";
            break;
          case 52:name = "Seven Out";
            break;
          case 53:name = "Easy Eight";
            break;
          case 54:name = "Nine";
            break;
          case 55:name = "Hard Ten";
            break;
          case 56:name = "Yo-leven";
            break;
            
          case 61:name = "Seven Out";
            break;
          case 62:name = "Easy Eight";
            break;
          case 63:name = "Nine";
            break;
          case 64:name = "Easy Ten";
            break;
          case 65:name = "Yo-leven";
            break;
          case 66:name = "Boxcars";
            break;
        }//end of sum switch
        System.out.print("Numbers where:" + dice1 + " and " + dice2 + "!\n" +
                        "The slang term is " + name);//prints answer
        break;
      case 0://two given numbers
        
        System.out.print("Number 1:");//asks for number
        int dice3 = myScanner.nextInt();//looks for integer
        System.out.print("Number 2:");//asks for number
        int dice4 = myScanner.nextInt();//looks for integer
        int sum2 = dice3 * 10 + dice4;//sums the numbers to easier identify them(16 = 1 and 6)
        String name2 = "";
        
        switch(sum2){
          case 11:name2 = "Snake Eyes";
            break;
          case 12:name2 = "Ace Deuce";
            break;
          case 13:name2 = "Easy Four";
            break;
          case 14:name2 = "Fever Five";
            break;
          case 15:name2 = "Easy Six";
            break;
          case 16:name2 = "Seven Out";
            break;
            
          case 21:name2 = "Ace Deuce";
            break;
          case 22:name2 = "Hard Four";
            break;
          case 23:name2 = "Fever Five";
            break;
          case 24:name2 = "Easy Six";
            break;
          case 25:name2 = "Seven Out";
            break;
          case 26:name2 = "Easy Eight";
            break;
            
          case 31:name2 = "Easy Four";
            break;
          case 32:name2 = "Fever Five";
            break;
          case 33:name2 = "Hard Six";
            break;
          case 34:name2 = "Seven Out";
            break;
          case 35:name2 = "Easy Eight";
            break;
          case 36:name2 = "Nine";
            break;
            
          case 41:name2 = "Fever Five";
            break;
          case 42:name2 = "Easy Six";
            break;
          case 43:name2 = "Seven Out";
            break;
          case 44:name2 = "Hard Eight";
            break;
          case 45:name2 = "Nine";
            break;
          case 46:name2 = "Easy Ten";
            break;
            
          case 51:name2 = "Easy Six";
            break;
          case 52:name2 = "Seven Out";
            break;
          case 53:name2 = "Easy Eight";
            break;
          case 54:name2 = "Nine";
            break;
          case 55:name2 = "Hard Ten";
            break;
          case 56:name2 = "Yo-leven";
            break;
            
          case 61:name2 = "Seven Out";
            break;
          case 62:name2 = "Easy Eight";
            break;
          case 63:name2 = "Nine";
            break;
          case 64:name2 = "Easy Ten";
            break;
          case 65:name2 = "Yo-leven";
            break;
          case 66:name2 = "Boxcars";
            break;
          default:
            System.out.print("One or more of your numbers is not in the given range(1-6), please restart program\n");
            name2 = "Error";
            break;
        }//end of sum switch
        System.out.print("Numbers where:" + dice3 + " and " + dice4 + "!\n" +
                        "The slang term is " + name2);//prints answer
        break;
    }//end of answer switch
  }//end main method
}//end class