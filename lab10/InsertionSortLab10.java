//insertion sort by Alan Zarza
import java.util.Arrays;
public class InsertionSortLab10{
  public static void main(String[]args){
    int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
    int[] myArrayWorst = {9,8,7,6,5,4,3,2,1};
    int iterBest = insertionSort(myArrayBest);
    int iterWorst = insertionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    System.out.println("The total number of operations performed on the reverse sorted array: "+ iterWorst);
  }//end main method
  public static int insertionSort(int[] list){
    System.out.println(Arrays.toString(list));
    int iterations = 0;//initalize counter for iterations
    for(int i =  0; i < list.length; i++){//for each element
      iterations ++;//update counter
      for(int j = i; j > 0; j--){//check elements in subgroup
        if(list[j] < list[j-1]){//if the element before list[j] is bigger
          int temp = list[j];//swap values
          list[j] = list[j-1];
          list[j-1] = temp;
          iterations ++;
        }
        else{//else do nothing, exit loop
          break;
        }
      }
      System.out.println(Arrays.toString(list));//print out each array
    }
    System.out.println(Arrays.toString(list));//print out final array
    return iterations;//return the number of iterations
  }
}//end of class