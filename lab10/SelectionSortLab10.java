import java.util.Arrays;
public class SelectionSortLab10{
  public static void main(String [] args){
    int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
    int[] myArrayWorst = {9,8,7,6,5,4,3,2,1};
    int iterBest = selectionSort(myArrayBest);
    int iterWorst = selectionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    System.out.println("The total number of operations performed on the reverse sorted array: "+ iterWorst);
  }//end main method
  /*Method for sorting numbers*/
  public static int selectionSort(int[] list){
    System.out.println(Arrays.toString(list));//prints initial array
    int iterations = 0;//initialize counter of iterations
    for(int i = 0; i<list.length;i++){//for each element in the array
      int currentMin = list[i];
      int currentMinIndex = i;
      iterations ++;//update iterations counter
      for(int j = i+1; j< list.length; j++){
        if(list[j]<currentMin){//check each element in subarray, if its smaller than previous small
          currentMin = list[j];//then we have a new min
          currentMinIndex = j;
        }
        iterations ++;//update iterations
      }
      if(currentMinIndex != i){//if there is a new min
        int temp = list[i];//then swap values
        list[i] = list[currentMinIndex];
        list[currentMinIndex] = temp;
      }
      System.out.println(Arrays.toString(list));//print array each time go through an iteration
    }
    System.out.println(Arrays.toString(list));//print final array
    return iterations;//returns numbers of iteratiosn
  }
}