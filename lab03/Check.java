//Check, by Alan Zarza
//uses the Scanner class to obtain from the user the original cost of the check,
//the percentage tip they wish to pay, and the number of ways the chack will be split
//Then determine how much each person in the group needs to spend in order to pay the check
import java.util.Scanner;

public class Check{//Check class
  
  public static void main(String[]args){//mian method
    
    Scanner myScanner = new Scanner( System.in );//Declaring instance of scanner object and calling Scanner construct
    
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):  ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;//We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;//dollars for dollars, coins for digits to the right of decimal
    totalCost = checkCost * (1 + tipPercent);//cost with tax
    costPerPerson = totalCost / numPeople;//dividing up the cost
    dollars=(int)costPerPerson;//number of dollars needed
    dimes=(int)(costPerPerson * 10) % 10;//number of dimes
    pennies=(int)(costPerPerson * 100) % 10;//number of pennies
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);





    
    
  }//end of main method
}//end of class