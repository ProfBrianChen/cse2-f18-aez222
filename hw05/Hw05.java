/*Hw05 by Alan Zarza
 *input: number of hands you want to generate
 *output: probability of drawing four-of-a-kind,
 *three-of-a-kind, two pair, and one pairs for
 *number of hands you wanted
 */

import java.util.Scanner;//import scanner

public class Hw05{//class
  
  public static void main(String[]args){//main method
    
    Scanner scan = new Scanner(System.in);//declare instance of scanner
    
    int hands = 0;//declare hands in main instance
    double fourOfAKind = 0.0;//declare four of a kind
    double threeOfAKind = 0.0;//declare three of a kind
    double twoPair = 0.0;//declare two pairs
    double onePair = 0.0;//declare one pairs
    do{//do the stuff in the body of the loop first
      System.out.print("The number of loops: ");//ask for integer
      boolean check = scan.hasNextInt();//checks to see if its integer
      if (check == true){//if its integer
        hands = scan.nextInt();//it sets hands as the input
        break;//and exits the loop
      }
      else{//if its not an integer
        scan.next();//it clears the scanned value
        System.out.println("Invalid Input, need integer");//and informs user of invalid input
      }
    }while(true);//the loop continues until check == true, which breaks the loop and gives value to hands
    
    for(int i = 1;i <= hands;++i){//loop for number of hands
      
      int draw1, draw2, draw3, draw4, draw5;//declares the draws
      
      draw1 = (int)(Math.random()*52)+1;//draws first card
      draw2 = (int)(Math.random()*52)+1;//draws second card
      draw3 = (int)(Math.random()*52)+1;//draws third card
      draw4 = (int)(Math.random()*52)+1;//draws fourth card
      draw5 = (int)(Math.random()*52)+1;//draws fifth card

      while (draw2 == draw1){//if draw 2 is the same as draw 1
        draw2 = (int)(Math.random()*52)+1;//then draw a new card
      }
      while ((draw3 == draw1) || (draw3 == draw2) ){//if draw3 is the same as draw1 or draw2
        draw3 = (int)(Math.random()*52)+1;//then draw again
      }
      while ((draw4 == draw1) || (draw4 == draw2) || (draw4 == draw3) ){//if draw4 is the same as previous cards
        draw4 = (int)(Math.random()*52)+1;//then draw again
      }
      while ((draw5 == draw1) || (draw5 == draw2) ||
            (draw5 == draw3) || (draw5 == draw4) ){//if draw5 is the same as any previous draw
        draw5 = (int)(Math.random()*52)+1;//then draw again
      }
      
      draw1 = draw1%13;//assigning face value
      draw2 = draw2%13;//assigning face value
      draw3 = draw3%13;//assigning face value
      draw4 = draw4%13;//assigning face value
      draw5 = draw5%13;//assigning face value
      
      int ace = 0;//declaring pile for aces
      int two = 0;//declaring pile for twos
      int three = 0;//declaring pile for threes
      int four = 0;//declaring pile for fours
      int five = 0;//declaring pile for fives
      int six = 0;//declaring pile for sixes
      int seven = 0;//declaring pile for sevens
      int eight = 0;//declaring pile for eights
      int nine = 0;//declaring pile for nines
      int ten = 0;//declaring pile for tens
      int jack = 0;//declaring pile for jacks
      int queen = 0;//declaring pile for queens
      int king = 0;//declaring pile for kings
      
      for(int e=1;e<=5;++e){//loop where we check what type of card we have and add it to the pile corresponding pile
        
        int card = 0;//declare card
        
        if(e==1){//if were on first iteration
          card = draw1;//card is first draw
        }
        else if(e==2){//if were on second iteration
          card = draw2;//card is second draw
        }
        else if(e==3){//if were on third iteration
          card = draw3;//card is third draw
        }
        else if(e==4){//if were on fourth iteretion
          card = draw4;//card is fourth draw
        }
        else if(e==5){//if were on fifth iteration
          card = draw5;//card is fifth draw
        }
      
        
      if(card==0){//depending on remainder
        king = ++king;//add to face pile
      }
      else if(card==12){//depending on remainder
        queen = ++queen;//add to face pile
      }
      else if(card==11){//depending on remainder
        jack = ++jack;//add to face pile
      }
      else if(card==10){//depending on remainder
        ten = ++ten;//add to face pile
      }
      else if(card==9){//depending on remainder
        nine = ++nine;//add to face pile
      }
      else if(card==8){//depending on remainder
        eight = ++eight;//add to face pile
      }
      else if(card==7){//depending on remainder
        seven = ++seven;//add to face pile
      }
      else if(card==6){//depending on remainder
        six = ++six;//add to face pile
      }
      else if(card==5){//depending on remainder
        five = ++five;//add to face pile
      }
      else if(card==4){//depending on remainder
        four = ++four;//add to face pile
      }
      else if(card==3){//depending on remainder
        three = ++three;//add to face pile
      }
      else if(card==2){//depending on remainder
        two = ++two;//add to face pile
      }
      else if(card==1){//depending on remainder
        ace = ++ace;//add to face pile
      }
      }//end loop where we check what type of card we have and add it to the pile corresponding pile
      int pairs = 0;//declair and intitialize pairs variable
      for(int e=1;e<=13;++e){//loop where we check for four, threes, and number of pairs in hand
        int pileSize = 0;//declare and initialize size of pile
        switch(e){//switches pile for each iteration
          case 1: pileSize = ace;
            break;
          case 2: pileSize = two;
            break;
          case 3: pileSize = three;
            break;
          case 4: pileSize = four;
            break;
          case 5: pileSize = five;
            break;
          case 6: pileSize = six;
            break;
          case 7: pileSize = seven;
            break;
          case 8: pileSize = eight;
            break;
          case 9: pileSize = nine;
            break;
          case 10: pileSize = ten;
            break;
          case 11: pileSize = jack;
            break;
          case 12: pileSize = queen;
            break;
          case 13: pileSize = king;
            break;
        }//end of switch statement
        if(pileSize == 4){//if there are four matching cards
          fourOfAKind = ++ fourOfAKind;//then increment fourOfAKind
        }
        else if(pileSize == 3){//if there are three cards with the same face
          threeOfAKind = ++threeOfAKind;//then increment threeOfAKind
        }
        else if(pileSize == 2)//if there is a pair
          pairs = ++pairs;//add it to the number of pairs we have in that hand
        
      }//end of loop
      
      if(pairs == 2){//if we had two pairs in the hand
        twoPair = ++twoPair;//then increment twoPair
      }
      else if(pairs == 1){//if we only had one pair
        onePair = ++onePair;//then increment onePair
      }
     pairs = 0;//don't forget to reset pairs for each hand
    
    }//end of (hands) for loop
    
    fourOfAKind = fourOfAKind/hands;//divide by number of hands to find probability
    threeOfAKind = threeOfAKind/hands;//divide by number of hands to find probability
    twoPair = twoPair/hands;//divide by number of hands to find probability
    onePair = onePair/hands;//divide by number of hands to find probability
    
    System.out.printf("The probability of Four-of-a-kind: %.3f\n",fourOfAKind );//probability of four of a kind
    System.out.printf("The probability of Three-of-a-kind: %.3f\n",threeOfAKind);//probability of three of a kind
    System.out.printf("The probability of Two-pair: %.3f\n", twoPair);//probability of two pair
    System.out.printf("The probability of One-pair: %.3f\n", onePair);//probability of one pair
    
  }//end of main method
}//end of class