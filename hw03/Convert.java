//Convert, by Alan Zarza
//input: acres of land, inches of rain
//output: rain in cubic miles
import java.util.Scanner;//importing the scanner utility

public class Convert{//convert class
  
  public static void main(String[]args){//main method
    
    Scanner myScanner = new Scanner( System.in );//declaring instance of myScaner
    
    double acres;//declaring acres
    double inches;//declaring inches
    double gallons;//declaring gallons
    double cubicMiles;//declaring cubic miles
    final double GALLON_CONVERSION_NUMBER = 27154;//one inch on an acre equals about 27,154 gallons
    final double MILE_CONVERSION_NUMBER = 9.0817e-13;//one us liquid gallon is equal to 9.0817e-13 cubic miles
    
    System.out.print("Enter the affected area in acres:");//asking for acres
    acres = myScanner.nextDouble();//checking for the value
    System.out.print("Enter the rainfall in the affected area:");//asking for inches of rain
    inches = myScanner.nextDouble();//checking for the value
    
    gallons = GALLON_CONVERSION_NUMBER * inches * acres;//calculating gallons
    cubicMiles = gallons * MILE_CONVERSION_NUMBER;//calculating cubic miles
    
    System.out.print(cubicMiles + "cubic miles");
    
  }//end main method
}//end class