//Pyramid by Alan Zarza
//input: length of square side, height
//output: volume of pyramid

import java.util.Scanner;//importing scanner utility

public class Pyramid{//Pyramid class
  
  public static void main(String[]args){//main method
    
    Scanner myScanner = new Scanner(System.in);//declaring instance of myScanner
    
    double length;//declaring the length double
    double height;//declaring the height double
    double volume;//declaring the volume double
    
    System.out.print("The square side of the pyramid is (input lenght):");//asks for side lenght of pyramid
    length = myScanner.nextDouble();//checks for input
    System.out.print("The height of the pyramid is (input height):");//asks for heigth of pyramid
    height = myScanner.nextDouble();//checks for input
    
    volume = (height * length *length)/3;//calculating volume
    
    System.out.print("The volume inside the pyramid is: " + volume);//prints out volume
    
    
  }//end of main method
}//end of class