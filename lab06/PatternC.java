/*PatternC by Alan Zarza
 *Makes pyramid out of number of input
 */

import java.util.Scanner;//import scanner

public class PatternC{//public class
  
  public static void main(String[]args){//main method
    
    Scanner scan = new Scanner(System.in);//declare scanner
    
    int length = 0;//declaring variable for length of pyramid
    
    boolean check = false;//assume we dont start of with an integer
    while(check == false){//while we don't have an integer
      System.out.println("Input an integer between 1 and 10");//ask for integer
      check = scan.hasNextInt();//check if integer
      if(check == true){//if it is an integer
        length = scan.nextInt();//then get the input and save it as lengh
        break;//and leave the loop
      }
      else{//otherwise
        scan.next();//reset the scanner
        System.out.println("INVALID INPUT");//inform user of invalid input
      }
    }//end of while loop
    
    for(int numRows = 1;numRows <= length; numRows++){//for length - 1
      for(int i = 10; i >= numRows; i--){
          System.out.print(" ");
        }
      for(int j = numRows; j >= 1; j--){//for 1-numRows
        System.out.print(""+j);//print out the number without making new lines
      }
      System.out.println();//make a new line
    }
    
  }//end main method
}//end class