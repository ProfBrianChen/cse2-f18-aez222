/*Arithmetic
 *by Alan Zarza
 */
//Arithmetic class
public class Arithmetic{
  //main method
  public static void main (String args[]){
    
    int numPants      = 3;//number of pairs of pants
    double pantsPrice = 34.98;//cost per pair of pants
    int numShirts     = 2;//number of shirts
    double shirtPrice = 24.99;//cost per shirt
    int numBelts      = 1;//number of belts
    double beltCost   = 33.99;//cost per belt

    double paSalesTax = 0.06;//Pennsylvania sales tax
    
    double totalPantsCost;//total cost of pairs of pants
    double taxPantsCost;//sales tax charged on pairs of pants 
    double totalShirtsCost;//total cost of shirts
    double taxShirtsCost;//sales tax charged on  shirts
    double totalBeltsCost;//total cost of belts
    double taxBeltsCost;//sales tax charged on belts
    double totalCost;//total cost without sales tax
    double totalTax;//total  sales tax charged
    double totalSaleCost;//total cost of sale plus sale tax
    
    totalPantsCost = numPants * pantsPrice;//calculates the cost of all the pants
    taxPantsCost = (double)((int)(totalPantsCost * paSalesTax * 100))/100;//calculates the taxes on the pants with only two numbers after the decimal
    totalShirtsCost = numShirts * shirtPrice;//calculates the cost of all the shirts
    taxShirtsCost = (double)((int)(totalShirtsCost * paSalesTax * 100))/100;//calculates the taxes on the pants with only two numbers after the decimal
    totalBeltsCost = numBelts * beltCost;//calculates the total cost of all belts
    taxBeltsCost = (double)((int)(totalBeltsCost * paSalesTax * 100))/100;//calculates the taxes on the belts with only two numbers after the decimal
    totalCost = totalPantsCost + totalShirtsCost + totalBeltsCost;//calculates the cost of all items without tax
    totalTax = taxPantsCost + taxShirtsCost + taxBeltsCost;//calculates the total taxes on all itmes
    totalSaleCost = totalCost + totalTax;//calculates the cost of all items plus taxes
    
    //Prints out above values in the style of a sales receipt
    System.out.println("           Total    Tax");
    System.out.println("-------------------------");
    System.out.println("Pants x" + numPants + "   $" + totalPantsCost + "  $" + taxPantsCost);
    System.out.println("Shirts x" + numShirts + "  $" + totalShirtsCost + "   $" + taxShirtsCost);
    System.out.println("Belts x" + numBelts + "   $" + totalBeltsCost + "   $" +taxBeltsCost);
    System.out.println("-------------------------");
    System.out.println("           $" + totalCost + "  $" + totalTax);
    System.out.println("-------------------------");
    System.out.println("Total:              $" + totalSaleCost);
    System.out.println("");
    System.out.println("");
    
  }//end of main method
}//end of class