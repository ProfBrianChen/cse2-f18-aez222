//Card Generator by Alan Zarza
//Generates number between 1 and 52
//Depending on number, it prints out
//the suit and identity of card

public class CardGenerator{//card generator class
  
  public static void main(String[]args){//main method
    
    int randGen = (int)(Math.random()*51)+1;//generates number between 1 - 52
    String suitName = "";//declaring suitName
    String identity = "";//declaring identity
    
    System.out.println("Number = " + randGen);//REMOVE
    
    if(randGen<=13){//diamonds
      suitName = "diamonds";//number is already ready for 
    }
    else if(randGen<=26){//clubs
      suitName = "clubs";
      randGen -= 13;//gets number ready for the switch statement
    }
    else if(randGen<=39){//hearts
      suitName = "hearts";
      randGen -= 26;//gets number ready for the switch statement
    }
    else if(randGen<=52){//spades
      suitName = "spades";
      randGen -= 39;//gets number ready for the switch statement
    }
    else{//if randGen gives number not expected
      System.out.println("Error with randGen");
    }
    
    //gives the card it's identity
    switch (randGen){
        case 1: identity = "Ace";
        break;
        case 2: identity = "Two";
        break;
        case 3: identity = "Three";
        break;
        case 4: identity = "Four";
        break;
        case 5: identity = "Five";
        break;
        case 6: identity = "Six";
        break;
        case 7: identity = "Seven";
        break;
        case 8: identity = "Eight";
        break;
        case 9: identity = "Nine";
        break;
        case 10: identity = "Ten";
        break;
        case 11: identity = "Jack";
        break;
        case 12: identity = "Queen";
        break;
        case 13: identity = "King";
        break;
    }
    
    //suitName = "";
    System.out.println("suit = " + suitName);//REMOVE
    System.out.println("identity = " + randGen);//REMOVE
    System.out.println(identity + " of " + suitName);
    
  }//main method end
}//class end