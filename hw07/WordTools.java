/*WordTools by Alan Zarza
 *Multiple tools for changing
 *the properties of a string
 */
import java.util.Scanner;

public class WordTools{//public class
  
  public static String sampleText(Scanner scnr){
    System.out.println("Enter a String of your choosing:\n");//asks for user input
    String sentence = scnr.nextLine();//checks for user input
    return sentence;//returns the user input
  }//prompts for String, returns string
  
  public static void printMenu(Scanner scnr, String input){
    System.out.println("\nMenu\n\n"+
                       "C - Number of non-whitespace characters\n"+
                       "W - Number of words\n"+
                       "F - Find text\n"+
                       "R - Replace all !'s\n"+
                       "S - Shorten spaces\n"+
                       "Q - Quit\n");//prints out the menu
    
    String word = scnr.next();//scans for input
    if(word.equals("Q")||word.equals("q")){//if user wants to quit
      System.out.println("\nOkay :-(");//only prints out message
    }
    else if(word.equals("C")||word.equals("c")){
      System.out.println("\nYou have "+getNumOfNonWSCharacters(input)+" non white space characters");//prints number of characters
      printMenu(scnr,input);//goes back to menu
    }
    else if(word.equals("W")||word.equals("w")){
      System.out.println("\nNumber of words: "+getNumOfWords(input));//prints number of words
      printMenu(scnr, input);//goes back to menu
    }
    else if(word.equals("F")||word.equals("f")){
      System.out.println("\nEnter a word or phrase to be found:\n");//ask for input
      String find = scnr.next();//check the input
      System.out.println("\n"+find+" instances: "+findText(find,input)+"\n");//prints out the number of times the text comes up
      printMenu(scnr,input);//goes back to menu
    }
    else if(word.equals("R")||word.equals("r")){
      System.out.println("\n"+replaceExclamation(input));//prints input with replaces exclamation points
      printMenu(scnr,input);//goes back to menu
    }
    else if(word.equals("S")||word.equals("s")){
      System.out.println("\n"+shortenSpace(input));//prints sentence with double spaces replaced with single spaces
      printMenu(scnr, input);//goes back to menu
    }      
    else{//if we don't get any valid input
      System.out.println("\nInvalid input");//notify user of invalid input
      printMenu(scnr,input);//run the menu method again, until we get a valid input
    }
  }//makes menu that calls itseld recursively until valid input is given
  
  public static int getNumOfNonWSCharacters(String input){
    int numCharacters = input.length();//this is our counter, starting at total number of characters
    
    for(int x = 0; x <= input.length()-1;x++){//now we check each character
      if(Character.isWhitespace(input.charAt(x))){//if it is whitespace
        numCharacters -= 1;//then we take one off our counter
      }
    }
    
    return numCharacters;//now we return the value of our counter
  }//counts number of non whitespace characters
  
  public static int getNumOfWords(String input){
    String sentence = shortenSpace(input);//make all double spaces into single, so we can count them for the number of words
    int numSpaces = 0;//this is our counter
    for(int x = 0;x<sentence.length();x++){
      if(Character.isWhitespace(sentence.charAt(x))){//if there are any spaces
        numSpaces += 1;//increment the number of spaces
      }
    }
    int numWords = numSpaces + 1;//in a sentence, there are always one more word then there are spaces
    return numWords;
  }//gets number of words by counting number of spaces
  
  public static int findText(String find, String input){
    
    int location = input.indexOf(find);//find where is find
    int count = 0;//counter
    while (location != -1) {//while find exists in the input
      input = input.substring(location + 1);//change input to start off after the location of last find
      location = input.indexOf(find);//find the new incex of find to compare again
      count += 1;//increment counter
    }
    return count;//return the count
  }//counts the number of times a word comes up in the input
  
  public static String replaceExclamation(String input){
    input = input.replaceAll("!",".");
    return input;
  }//replaces exclamation points with periods
  
  public static String shortenSpace(String input){
    
    while(input.indexOf("  ")!=-1){//while there are whitespaces in the sentence
      input = input.replaceAll("  "," ");//go and replace doublespaces with a singlespace
    }
    return input;//returns the new sentence
  }//replaces all instances of double space or more with a single space
  
  
  public static void main(String[]args){//main method
    Scanner scnr = new Scanner(System.in);//declaring new instance of scanner
    
    String input = sampleText(scnr);//sets the input for the menu and the rest of methods to use
    printMenu(scnr, input);//starts up the menu
     
     
  }//end of main method
}//end of public class