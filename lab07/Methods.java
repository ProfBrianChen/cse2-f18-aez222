/*Methods by Alan Zarza
 *Makes sentences out of premade databases
 */
import java.util.Random;//importing random class
import java.util.Scanner;//importing scanner class

public class Methods{//public class
  
  public static String adj(){
    Random randNum = new Random();//create instance of randNum
    int num = randNum.nextInt(10);//declare and generate randum number between 0-9
    
    switch(num){//for each case, return certain value
      case 0: return "brown";
      case 1: return "cool";
      case 2: return "small";
      case 3: return "big";
      case 4: return "dark";
      case 5: return "moody";
      case 6: return "tall";
      case 7: return "green";
      case 8: return "speckled";
      case 9: return "fierce";
      default: return "Error";
    }//end switch
    
  }//end adj
 
  public static String subNoun(){
    Random randNum = new Random();//creaate instance of randNum
    int num = randNum.nextInt(10);//declare random number between 0-9
    
    switch(num){//for each case, return certain value
      case 0: return "dog";
      case 1: return "cat";
      case 2: return "giant";
      case 3: return "monster";
      case 4: return "man";
      case 5: return "boy";
      case 6: return "baby";
      case 7: return "puppy";
      case 8: return "proffesor";
      case 9: return "student";
      default: return "Error";
    }//end switch
  }//end subNoun
  
  public static String verb(){
    Random randNum = new Random();//creaate instance of randNum
    int num = randNum.nextInt(10);//declare random number between 0-9
    
    switch(num){//for each case return certain value
      case 0: return "ran";
      case 1: return "jumped";
      case 2: return "swung";
      case 3: return "screamed";
      case 4: return "fell";
      case 5: return "walked";
      case 6: return "sat";
      case 7: return "slept";
      case 8: return "worked";
      case 9: return "drew";
      default: return "Error";
    }//end switch
  }//end verb
  
  public static String objNoun(){
    Random randNum = new Random();//creaate instance of randNum
    int num = randNum.nextInt(10);//declare random number between 0-9
    
    switch(num){//for each case return certain value
      case 0: return "park";
      case 1: return "classroom";
      case 2: return "pool";
      case 3: return "sidewalk";
      case 4: return "skating rink";
      case 5: return "bench";
      case 6: return "bush";
      case 7: return "morge";
      case 8: return "church";
      case 9: return "dorm";
      default: return "Error";
    }//end switch
  }//end objNoun
  
  public static String adverb(){
    Random randNum = new Random();//creaate instance of randNum
    int num = randNum.nextInt(10);//declare random number between 0-9
    
    switch(num){//returns ceratain value for each case
      case 0: return "easily";
      case 1: return "jouyously";
      case 2: return "promptly";
      case 3: return "silently";
      case 4: return "madly";
      case 5: return "defiantly";
      case 6: return "mortaly";
      case 7: return "mechanicaly";
      case 8: return "slowly";
      case 9: return "quickly";
      default: return "Error";
    }//end switch
  }//end adverb
  
  public static String thesis(){
    String subject = subNoun();//save subjNoun as subject
    System.out.println("The "+adj()+" "+adj()+" "+subject+" "+verb()+" in the "+objNoun());//print the thesis sentence
    return subject;//returns the subject of the sentence
  }//end thesis
  
  public static void body(String subject){
    
    Random randNum = new Random();//creaate instance of randNum
    int num = randNum.nextInt(2);//generate random number 0-1
    
    String sub = (num==1)? ("The "+ subject):"It";//if num is 1, then sub is "the subject", other wise it's "it"
    
    System.out.println("This "+subject+" was "+adverb()+" "+adj()+" to the "+objNoun()+".");//print a sentence
    System.out.println(sub+" "+verb()+" it's "+ objNoun()+"s "+adj()+".");//print another sentence that uses "it" sometimes
  }//end subject
  
  public static void conclusion(String subject){
    System.out.println("The "+subject+" "+verb()+" it's "+ objNoun()+"s "+adj()+".");//print sentence
  }//end conclusion
  
  public static void main(String[]args){//main method
  
    Scanner scan = new Scanner(System.in);
    boolean check = true;//asume we start of with not a string
    while(check==true){//as long as we don't have a string
      System.out.println("Would you like me to generate a sentence?(Y or N):");//ask for input
      check = scan.hasNextInt();//check if input is an integer
      if(check == false){//if input is not an integer
        check = scan.hasNextDouble();//check if input is a double
        if (check==false){//if input is not a double
          String ans = scan.next();//scan the input and save as ans
          if(ans.equals("Y")||ans.equals("y")||ans.equals("Yes")||ans.equals("yes")||ans.equals("YES")){//if the input says yes
            String subject = thesis();//print the thesis sentence
            body(subject);
            conclusion(subject);
            check = true;//make check true to repeate the loop
          }
          else if(ans.equals("N")||ans.equals("n")||ans.equals("No")||ans.equals("no")||ans.equals("NO")){//if the input says no
            System.out.println("Okay :-(");//just print a sad face, since check is false already, we will exit loop here
          }
          else{
            System.out.println("Invalid Input3");//inform of invalid input
            check = true;//set check to true to repeate the loop
          }
          //System.out.println("Correct");
        }
        else{//if input is a double
          System.out.println("Invalid Input2");//inform of invalid input
          scan.next();//reset the scanner
        }
      }
      else{//if input is an integer
        System.out.println("Invalid Input1");//inform of invalid input
        scan.next();//reset scanner
      }
    }//end while loop
    
  }//end main method
}//end public class