/*hw08 by Alan Zarza
 *shuffles deck of cards and draws hands
 *keeps drawing hands as long as user inputs 1
 *makes new deck when it runs out of cards
 */
import java.util.Scanner;
  public class hw08{//public class (changed the name to match name of file)
    
    public static String[] shuffle(String[] list){
      int numShuffles = 100;//how many times we shuffle
      for(int i=0; i<numShuffles;i++){//for numbers 0 until we dont want to shuffle anymore
        int pos = (int)(Math.random()*52);//make a random number between 0 and 51
        String temp = list[0];//put string from first index in temporary variable
        list[0] = list[pos];//firsst index gets value from new index
        list[pos] = temp;//new index gets value from first index
      }
      return list;//returns new list
    }//shuffles array
    
    public static String[] getHand(String[] list, int index, int numCards){
      String[] hand = new String[numCards];
      for(int i = 0; i<numCards;i++){
        hand[i] = list[index-i];
      }
      return hand;
    }//gets a hand of cards from back of deck
    
    public static void printArray(String[] list){
      for(int i = 0; i < list.length;i++){//for each element
        System.out.print(list[i]+" ");//print it out
      }
    }//prints out the array
    
    public static void main(String[] args) {//main method
      
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; //making array for cards
    String[] hand = new String[5]; //making array for hand
    int numCards = 5; //number of cards drawn each hand
    int again = 1; //used to check if user wants to draw another card
    int index = 51;//used to keep track of last card in deck
    for (int i=0; i<52; i++){ //for fifty two cards
      cards[i]=rankNames[i%13]+suitNames[i/13]; //give the cards their suit and rank
      System.out.print(cards[i]+" "); //inital print out
    } 
    System.out.println("\n");//space
    printArray(cards); //print original array of cards
    shuffle(cards);//shuffle the array
    System.out.println("\n");//space
    printArray(cards); //print out the new array of cards
    while(again == 1){//checking if user wants to keep drawing cards
      hand = getHand(cards,index,numCards);//geting a new hand
      System.out.println("\nHand:");//space
      printArray(hand);//print out the hand
      index = index - numCards;//decrease index by the number of cards we drew
      System.out.println();//space
      System.out.println("Enter a 1 if you want another hand drawn");//ask for intput 
      again = scan.nextInt();//check for the input
      if(numCards>index){//if we want to draw more cards than we have
        System.out.println("\nCreating new deck");
        index = 51;//reset the index
        shuffle(cards);//shuffle the cards again
        printArray(cards);//FIX ME
      }
    }//end while loop
  }//end main method 
}//end class
