/*userInput by Alan Zarza
 *outputs the user's input
 */

import java.util.Scanner;//imports the scanner

public class userInput{//class
  
  public static void main(String[]args){//main method
    
    Scanner scan = new Scanner(System.in);//declare instance of scanner object
    
    int courseNum = 0;//declare course number
    boolean Bool = false;//declare a boolean
    
    while(Bool == false){//while input isn't an integer
      System.out.print("Course Number(Integer); ");
      Bool = scan.hasNextInt();//check to see if its an integer
      if(Bool == true){//if input is an integer
        courseNum = scan.nextInt();//set courseNum to input
      }
      else{//if input is not an integer
        scan.next();//clear
        System.out.println("Invalid Input");
      }
    }//end of while statement
    
    String depName = "";//name of department
    Bool = true;//set bool to true
    boolean Bool2 = true;//declare second bool true
    while(Bool == true || Bool2 == true){//while input isn't an string
      System.out.print("Department Name(String); ");
      Bool = scan.hasNextInt();//check to see if its an string
      Bool2 = scan.hasNextDouble();//check to see if its a double
      if(Bool == false && Bool2 == false){//if input is an string
        depName = scan.next();//set depname to input
      }
      else{//if input is not a string
        scan.next();//clear
        System.out.println("Invalid Input");
      }
    }//end of while statement
    
    int numTimes = 0;//number of times they meet per week
    Bool = false;//set bool to false
    
    while(Bool == false){//while input isn't an integer
      System.out.print("Number of times you meet per week(Integer); ");
      Bool = scan.hasNextInt();//check to see if its an integer
      if(Bool == true){//if input is an integer
        numTimes = scan.nextInt();//set courseNum to input
      }
      else{//if input is not an integer
        scan.next();//clear
        System.out.println("Invalid Input");
      }
    }//end of while statement
    
    int classStart = 0;//time class starts
    Bool = false;//sets bool to false
    
    while(Bool == false){//while input isn't an integer
      System.out.print("Class start time(Military Time); ");
      Bool = scan.hasNextInt();//check to see if its an integer
      if(Bool == true){//if input is an integer
        classStart = scan.nextInt();//set courseNum to input
      }
      else{//if input is not an integer
        scan.next();//clear
        System.out.println("Invalid Input");
      }
    }//end of while statement
    
    String instructor = "";//declare instructor string
    Bool = true;//set bool to true
    
    while(Bool == true){//while input isn't an string
      System.out.print("Instructor Name(String last name); ");
      Bool = scan.hasNextInt();//check to see if its an string
      if(Bool == false){//if input is an string
        instructor = scan.next();//set depname to input
      }
      else{//if input is not a string
        scan.next();//clear
        System.out.println("Invalid Input");
      }
    }//end of while statement
    
    int numStudents = 0;//time class starts
    Bool = false;//set bool to false
    
    while(Bool == false){//while input isn't an integer
      System.out.print("Number of Students(Integer); ");
      Bool = scan.hasNextInt();//check to see if its an integer
      if(Bool == true){//if input is an integer
        numStudents = scan.nextInt();//set courseNum to input
      }
      else{//if input is not an integer
        scan.next();//clear
        System.out.println("Invalid Input");
      }
    }//end of while statement
    
    
    System.out.print("Course Number:"+ courseNum + "\nDepartment Name:" + depName + "\nMeetings per week:" + numTimes + "\nClass Start Time:" + classStart + "\nInstructor Name:" + instructor + "\nNumber of Students:" + numStudents + "\n");
    
  }//end of main method
}//end of class