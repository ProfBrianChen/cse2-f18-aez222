/*Arrays by Alan Zarza
 *puts random numbers in an array
 *then counts how many times each number comes up
 *and puts those values in another array
 *prints out how many times a number comes up
 */

public class Arrays{//start class
  
  public static void main(String[]args){//main method
   
    int[] numbers = new int[100];//create array for numbers
    int[] quant = new int[100];//create array for quantity of numbers
    
    for(int i = 0; i<100; i++){//for 0-99
      numbers[i] = (int)(Math.random()*100);//give each array value a random number 0-99
      for(int j = 0; j<100; j++){//for 0-99 each time we give a number
        if(numbers[i]==j){//check what number we have in this range
          quant[j] +=1;//increment the quantity value for that number
        }//end if
      }//end second for loop
    }//end first for loop
    
    System.out.print("Array 1 holds sthe following integers: ");
    for(int o = 0; o<100;o++){
      System.out.print(numbers[o]+" ");//prints all the nubers we got
    }
    System.out.println("");
    for(int f = 0; f<100; f++){//for all value between 0 and 99
      if(quant[f]>0){//only numbers that apear at leasst once
        System.out.println(f + " occurs "+ quant[f] + " times");//print how many times they occur
      }
    }//end for loop
    
  }//end class
}//end main method