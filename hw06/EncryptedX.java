/*EncryptedX by Alan Zarza
 *prints out hiden message x
 *given number between 0 and 100
 */

import java.util.Scanner;//importing scanner

public class EncryptedX{//class
  
  public static void main(String[]args){//main method
    
    Scanner scan = new Scanner(System.in);//declare instance of scanner
    
    boolean check = false;//boolean I use to check if input is correct(start off assuming we don't have correct value)
    int num = -1;//declaring num for user input(not between 0 and 100 to start of with)
    
    while(check == false){//loop repeats until the check is true
      System.out.println("Input an integer between 0 and 100:");//ask for number
      check = scan.hasNextInt();//check wether number is an integer
      if(check == true){//if number is an integer
        num = scan.nextInt();//see what number we have
        if(num < 0 || num > 100){//if number is not in the range 0-100
          check = false;//make the check false to repeat the loop again
          System.out.println("Error: Num in invalid range");//print out error message
        }
      }
      else{//if check is false
        scan.next();//clear the scanner
        System.out.println("Error: Invalid input");//error message
      }
    }//end of loop that checks if input is integer and between 0 and 100
    System.out.println("Input was:"+num);//FIXME:just for testing, make sure to delete
    
    //FIXME: do the loops that print out hidden message
    for(int numRows = 0; numRows < num; numRows++){//make rows until we reach num
      for(int numCol = 0; numCol < num; numCol++){//make collums until we reach num
        if(numRows == numCol || num-(numRows+1) == numCol){//if the row we are in is the same as colum or the row from opposite side is same as colum
          System.out.print(" ");//print out a blank space
        }
        else{//otherwise
        System.out.print("*");//print out a star
        }
      }
      System.out.println("");//make a new row
    }//end of loop to print out message
  }//end main method
}//end public class