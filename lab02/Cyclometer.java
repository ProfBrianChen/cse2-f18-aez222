//Lab02
//by Alan Zarza
//Cyclometer
//prints number of minutes for each trip
//prints number of counts for each trip
//prints distance of each trip in miles
//prints distance for both trips combined
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
      int secsTrip1=480;  //time in seconds for trip 1
      int secsTrip2=3220;  //time in seconds for trip 2
		  int countsTrip1=1561;  //number of rotations of front wheel for trip 1
		  int countsTrip2=9037; //number of rotations of front wheel for trip 2
      double wheelDiameter=27.0,  //the wheel's diameter in inches
  	  PI=3.14159, // the pi constant
  	  feetPerMile=5280,  //number of feet per mile
  	  inchesPerFoot=12,   //number of inches per foot
  	  secondsPerMinute=60;  //number of seconds in a minute
	    double distanceTrip1, distanceTrip2,totalDistance;  //makes these variables into doubles for later
      System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
	    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles for trip 1
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;// Gives distance in miles for trip 2
	    totalDistance=distanceTrip1+distanceTrip2;// Gives total distance of both trips
      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");


	}  //end of main method   
} //end of class