/*lab09 by Alan Zara
 *inverts arrays and makes copies of arrays
 */
public class lab09{//start class
  
  public static int[] copy(int[] input){
    int[] newArray = new int[input.length];//make new array of same size as input array
    for(int i = 0; i < input.length;i++){//for each value in old array
      newArray[i] = input[i];//make new array value equal to old array value
    }
    return newArray;//return the new array
  }//make a copy of an array
  
  public static void inverter(int[] input){
    for(int i = 0; i<input.length/2;i++){//only switch until halfway through array
      int temp = input[i];//make a temporary value equal to first value
      input[i] = input[input.length-1-i];//switch first value with last value
      input[input.length-1-i] = temp;//make last value equal to first value that was saved in temp
    }
  }//inverts array
  
  public static int[] inverter2(int[] input){
    int[] copy = copy(input);//makes copy of input array
    inverter(copy);//inverts the copy
    return copy;//returns the copy
  }//inverst copy of array

  public static void print(int[] input){
    System.out.println("");
    for(int value:input){//for each loop
      System.out.print(value + " ");//prints out array values and spaces
    }
  }//prints out array values
  
  public static void main(String[]args){//start main method
    
    int[] array = {1,2,3,4,5,6,7,8,9,10,11};//new array
   
    int[] array0 = copy(array);//making copy
    int[] array1 = copy(array);//making copy
    int[] array2 = copy(array);//making copy
    inverter(array0);//inverts the actual array
    print(array0);//prints inverted array
    inverter2(array1);//makes copy of inverted array
    print(array1);//original array prints
    int[] Array3 = inverter2(array2);//assigns copy of inverted array
    print(Array3);//prints copy of inverted array
    
  }//end class
}//end main method