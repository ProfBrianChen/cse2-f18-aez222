/*CSE2Linear by Alan Zarza
 *does stuff with student's final grades
 *for CSE2
 */

import java.util.Scanner;
import java.util.Arrays;

public class CSE2Linear{
 
  public static void printArray(int[] input){
    for(int i = 0; i<input.length; i++){//for each element in array
      System.out.print(input[i]+" ");//print out the element 
    }
  }//prints out an array on one line
  public static void binarySearch(int[] input, Scanner scan ){
    int high = input.length;//seting first high point
    int low = 0;//setting first low point
    int mid = high/2;//setting first mid point
    int counter = 1;//setting counter
    System.out.print("\nEnter grade to search for:");//asking for key
    int key = scan.nextInt();//scan the key
    while(high>=low){//as long as the high number is higher than the low number
      if(key == input[mid]){//if the key is in the midpoint
        System.out.println(key+" was found with "+counter+" itirations");//print out message
        break;//and break out of the loop
      }
      else if(key > input[mid]){//if the key is higher value than the midpoint
        low = mid+1;//make the low point the old midpoint
      }
      else if(key < input[mid]){//if the key is lower value than the midpoint
        high = mid - 1;//make the highpoint the value of the old midpoint
      }
      mid = (high + low)/2;//update the midpoint value
      counter += 1;//increment the counter for number of itirations we have
    }
    if(high< low){//if the high is ever lower than the low
      System.out.println(key+" was not found in the list with "+counter+" iterations");//print out message
    }
  }//binary search and counting of iterations
  public static void linearSearch(int [] input, Scanner scan){
    System.out.println("\nEnter a grade to search for:");//ask for key
    int key = scan.nextInt();//scan for input
    int counter = 0;//set counter
    for(int i = 0; i<input.length; i++){//for each element of array
      counter += 1;//increment counter each time
      if(key == input[i]){//if the key is the same value as element
        System.out.println(key+" was found in "+ counter + " iterations");//print out mesage
      }
      else if(i == input.length){//if we run out of numbers
        System.out.println(key+" was not found in "+ counter +" iterations");//print out other message
      }
    }
  }//linear search and counting of iterations
  public static void scramble(int[] input){
    int numShuffles = 100;//number of shufles(make bigger for better results)
    for(int i = 0; i < numShuffles;i++){//shuffles numShuffles number of times
      int position = (int)(Math.random()*(input.length-1));//give random index in given input
      int temp = input[0];//assign temp value to first element
      input[0] = input[position];//assign value of element at random position to first element
      input[position] = temp;//assign value of first element to that random position element
    }
    System.out.println("Scrambled:");//print on new line
    printArray(input);//print out array
  }//scrambels array
  
  
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);//declaring new intstance of scanner
    int[] grades = new int[15];//declaring array of size 15 for grades
    System.out.println("Enter 15 ascending ints for final grades in CSE2");
    
    for(int i = 0; i<15; i++){
      boolean check = false;//start off assuming input is wrong
      while(check == false){//while the input is wrong
        check = scan.hasNextInt();//check the input
        if(check == true){//if the input is correct
          int temp = scan.nextInt();//make the input temp value
          if(temp<=100 && temp>=0){//and check if its in the correct range
            if(i==0){//now check if the index is 0
              grades[i] = temp;//if it is, then we dont worry about ascending order
            }
            else if(temp>=grades[i-1]){//now we check if the grades are in ascending order
              grades[i] = temp;//if they are, then we put the grade in with the rest of them
            }
            else{//if neither of these conditiones apply
              System.out.println("Error: Please enter numbers in ascending order");//print error message
              check = false;//set check as false, we dont need to clear up the scan since we already took the value 
            }
          }
          else{//if number isn't in correct range
            System.out.println("Error: Please enter an integer between 0-100");//print error message
            scan.next();//clear the scanner
            check = false;//set check as false
          }
        }
        else{//if input wasn't an integer
          System.out.println("Error: Please enter and integer");//print error message
          scan.next();//clear the scanner
        }
      }//end of while loop
      
    }//end for loop
    printArray(grades);//print array
    binarySearch(grades, scan);//search using binary method
    scramble(grades);//scramble the array
    linearSearch(grades, scan);//now search using the linear method
    
  }//end main method
}//end class