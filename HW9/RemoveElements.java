/*RemoveElemnt by Alan Zarza
 *a bunch of methods to mess around with methods
 */

import java.util.Scanner;
import java.util.Arrays;
public class RemoveElements{
  
  public static void main(String [] arg){
   
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"
      System.out.println(out1);
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"
      System.out.println(out2);
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
    
    
}
  
  public static String listArray(int num[]){
  String out="{";
  for(int j=0;j<num.length;j++){
    if(j>0){
    out+=", ";
    }
    out+=num[j];
  }
  out+="} ";
  return out;
}
  public static int[] randomInput(){
    int[] array = new int[10];//new array
    for(int i = 0; i<array.length;i++){//for each element in array
      array[i] = (int)(Math.random()*9);//assign element to random number between 0-9
    }
    return array;//return the randomny generated array
  }//generates random array of size 10, with integers between 9 and 10
  public static int[] delete(int[] list, int pos){
    if(pos<0||pos>9){//if pos is not in correct range
      System.out.println("The index is not valid");//notify user
      return list;//return original array
    }
    
    int[] newArray = new int[list.length-1];//make new array thats one element smaller
    for(int i = 0; i<pos; i++){//for each elemnet before the position
      newArray[i] = list[i];//repace the elements with ones in same position
    }
    for(int i = pos; i<newArray.length; i++){//for elements after the position
      newArray[i] = list[i+1];//replace with elements after the position
    }
    return newArray;//return this new array
  }//deletes element at specific position
  public static int[] remove(int[] list, int target){
    int counter = 0;//for counting how many elemnts to take away
    for(int i = 0; i<list.length; i++){//check each element
      if(list[i] == target){//if the element has target value
        counter += 1;//increment counter
      }
    }
    int[] newArray = new int[list.length-counter];//make new smaller array
    int pos = 0;//counter for position
    for(int i = 0; i< list.length; i++){//for each of original elements
      if(list[i]!=target){//if it is not the target value
        newArray[pos] = list[i];//add it into the array
        pos += 1;//and increment the position counter
      }
    }
    return newArray;//retur the new array
  }//FIX ME
}
